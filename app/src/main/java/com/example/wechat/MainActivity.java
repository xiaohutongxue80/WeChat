package com.example.wechat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Fragment hTab01 = new WeChatBlankFragment();
    private Fragment hTab02 = new FriBlankFragment();
    private Fragment hTab03 = new FoundBlankFragment();
    private Fragment hTab04 = new MeBlankFragment();

    private LinearLayout hTabWeChat;
    private LinearLayout hTabFrd;
    private LinearLayout hTabContact;
    private LinearLayout hTabMe;

    private ImageView imageWeixin,imagepengyou,imagefaxian, imagewo;

    private FragmentManager fm;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        textView=findViewById(R.id.textView2);

        hTabWeChat =findViewById(R.id.tab_weixin);
        hTabFrd =findViewById(R.id.tab_pengyou);
        hTabContact =findViewById(R.id.tab_faxian);
        hTabMe =findViewById(R.id.tab_shezhi);

        imageWeixin=findViewById(R.id.imageView);
        imagepengyou=findViewById(R.id.imageView1);
        imagefaxian=findViewById(R.id.imageView2);
        imagewo =findViewById(R.id.imageView3);

        initEvent();
        initFragment();
        showfragment(0);
    }


    private void initFragment() {
        fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.add(R.id.content, hTab01);
        transaction.add(R.id.content, hTab02);
        transaction.add(R.id.content, hTab03);
        transaction.add(R.id.content, hTab04);
        transaction.commit();
    }

    private void initView() {
        hTabWeChat = (LinearLayout) findViewById(R.id.tab_weixin);
        hTabFrd = (LinearLayout) findViewById(R.id.tab_pengyou);
        hTabContact = (LinearLayout) findViewById(R.id.tab_faxian);
        hTabMe = (LinearLayout) findViewById(R.id.tab_shezhi);

    }


    private void showfragment(int i){
        FragmentTransaction transaction=fm.beginTransaction();
        hideFragment(transaction);
        switch (i){
            case 0:
                textView.setText("微信");
                transaction.show(hTab01);
                imageWeixin.setImageResource(R.drawable.a_nomal);
                break;

            case 1:
                textView.setText("朋友");
                transaction.show(hTab02);
                imagepengyou.setImageResource(R.drawable.b_nomal);
                break;

            case 2:
                textView.setText("发现");
                transaction.show(hTab03);
                imagefaxian.setImageResource(R.drawable.c_nomal);
                break;

            case 3:
                textView.setText("我");
                transaction.show(hTab04);
                imagewo.setImageResource(R.drawable.d_nomal);
                break;

            default:
                break;
        }
        transaction.commit();
    }


    private void hideFragment(FragmentTransaction transaction) {
        transaction.hide(hTab01);
        transaction.hide(hTab02);
        transaction.hide(hTab03);
        transaction.hide(hTab04);

    }

    private void initEvent() {
        hTabWeChat.setOnClickListener(this);
        hTabFrd.setOnClickListener(this);
        hTabContact.setOnClickListener(this);
        hTabMe.setOnClickListener(this);
    }

    public void onClick(View v) {
        resetImage();
        switch (v.getId()){
            case R.id.tab_weixin:
                showfragment(0);
                break;
            case R.id.tab_pengyou:
                showfragment(1);
                break;
            case R.id.tab_faxian:
                showfragment(2);
                break;
            case R.id.tab_shezhi:
                showfragment(3);
                break;
            default:
                break;
        }

    }
    public void resetImage(){
        imageWeixin.setImageResource(R.drawable.a_original);
        imagepengyou.setImageResource(R.drawable.b_original);
        imagefaxian.setImageResource(R.drawable.c_original);
        imagewo.setImageResource(R.drawable.d_original);
    }
}